# About the application

The application is made in Laravel (version 8 or higher) and you need PHP version 7.3 and up to work.

# Development environment

The project can work in two ways in the development environment. With Docker or without docker.

### With Docker

To run through the docker, you must have the docker installed. Once installed, enter the project folder and run the following commands:

```
docker-compose up -d
docker exec -it good-game-challenge_app_1 bash
composer install
cp .env.example .env
php artisan key:generate
exit
```

After executing these commands, the application will be running locally at url localhost:8080

### Without docker

To run without the docker, you need to:

1. PHP 7.3^
2. Composer

With the above two requirements installed, just enter the project folder and run the following commands:

```
composer install
cp .env.example .env
php artisan key:generate
php artisan serve --port 8000
```

After executed these commands, the application will be running locally on url localhost:8000

# Production environment

The application was deployed on the Heroku platform. The host is https://good-game-challenge.herokuapp.com/

### Functionalities

To generate your army, just send a request to the route https://good-game-challenge.herokuapp.com/api/army (POST), with the parameter 'amount'.

# More informations

For more information, just access the WIKI below [link](https://bitbucket.org/leonardoperazzini/good-game-challenge/wiki/Home)