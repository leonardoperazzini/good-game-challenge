<?php

namespace Tests\Feature\Army;

use App\Services\ArmyService;
use App\Services\TroopService;
use App\Services\TypeTroopService;
use App\Models\Army;
use Tests\TestCase;

class ArmyServiceTest extends TestCase
{
    public function setUp(): void
    {
        $this->typeTroopService = new TypeTroopService();
        parent::setUp();
    }

    public function test_should_test_1000_times_return_army_with_amount_equals_sum_of_troops() {
        $faker = \Faker\Factory::create();

        $typesTroopsAvailable = $this->typeTroopService->getTypesTroops();

        for ($i=0; $i < 1000; $i++) { 
            $this->armyService = new ArmyService(new Army(),new TroopService(), new TypeTroopService());

            $amount = $faker->numberBetween(sizeof($typesTroopsAvailable),167);
            $army = $this->armyService->create($amount);
    
            $sum = 0;
            foreach ($army->getTroops() as $troop) {
                $sum += $troop->getAmount();
            }
    
            $this->assertEquals($amount, $sum);
        }
    }
}
