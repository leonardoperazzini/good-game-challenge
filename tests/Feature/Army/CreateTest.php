<?php

namespace Tests\Feature\Army;

use App\Services\TypeTroopService;
use Tests\TestCase;

class CreateTest extends TestCase
{
    public function setUp(): void
    {
        $this->typeTroopService = new TypeTroopService();
        parent::setUp();
    }

    public function test_should_return_army_with_amount_equals_sum_of_troops() {
        $faker = \Faker\Factory::create();

        $typesTroopsAvailable = $this->typeTroopService->getTypesTroops();

        $amount = $faker->numberBetween(sizeof($typesTroopsAvailable),167);
        $response = $this->post('/api/army', [ 'amount' => $amount ]);

        $sum = 0;
        foreach ($response->original->resource->getTroops() as $troop) {
            $sum += $troop->getAmount();
        }

        $this->assertEquals($amount, $sum);
        $response->assertStatus(201);
    }

    public function test_should_return_army_with_amount_one_of_each_troop() {
        $typesTroopsAvailable = $this->typeTroopService->getTypesTroops();

        $amount = sizeof($typesTroopsAvailable);
        $response = $this->post('/api/army', [ 'amount' => $amount ]);

        foreach ($response->original->resource->getTroops() as $troop) {
            $this->assertEquals($troop->getAmount(), 1);
        }

        $response->assertStatus(201);
    }
}
