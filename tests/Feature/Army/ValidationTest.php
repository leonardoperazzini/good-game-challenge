<?php

namespace Tests\Feature\Army;

use App\Services\TypeTroopService;
use Tests\TestCase;

class ValidationTest extends TestCase
{
    public function setUp(): void
    {
        $this->typeTroopService = new TypeTroopService();
        parent::setUp();
    }

    public function test_should_return_422_empty_field() {
        $typesTroopsAvailable = $this->typeTroopService->getTypesTroops();

        $response = $this->post('/api/army', []);

        $response->assertStatus(422);
        $response->assertJsonValidationErrors(['amount']);
        $response->assertExactJson([
            'errors' => [
                'amount' => [
                    'The amount field is required.'
                ]
            ]
        ]);
    }

    public function test_should_return_422_value_must_be_integer() {
        $response = $this->post('/api/army', [ 'amount' => 'amount' ]);

        $response->assertStatus(422);
        $response->assertJsonValidationErrors(['amount']);
        $response->assertExactJson([
            'errors' => [
                'amount' => [
                    'The amount must be an integer.'
                ]
            ]
        ]);
    }

    public function test_should_return_422_value_must_be_bigger() {
        $typesTroopsAvailable = $this->typeTroopService->getTypesTroops();

        $amount = sizeof($typesTroopsAvailable) - 1;
        $response = $this->post('/api/army', [ 'amount' => $amount ]);

        $response->assertStatus(422);
        $response->assertJsonValidationErrors(['amount']);
        $response->assertExactJson([
            'errors' => [
                'amount' => [
                    'Must be bigger than ' . $amount
                ]
            ]
        ]);
    }
}
