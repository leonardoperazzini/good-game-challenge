<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Contracts\Requests\Army\CreateRequestInterface;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind('App\Contracts\Services\ArmyServiceInterface', 'App\Services\ArmyService');
        $this->app->bind('App\Contracts\Services\TroopServiceInterface', 'App\Services\TroopService');
        $this->app->bind('App\Contracts\Services\TypeTroopServiceInterface', 'App\Services\TypeTroopService');

        $this->app->bind('App\Contracts\Models\ArmyInterface', 'App\Models\Army');

        $this->app->resolving(CreateRequestInterface::class, function ($teste, $app) {
            $teste->setTypeTroopServiceInterface(new \App\Services\TypeTroopService());
        });
    }
}
