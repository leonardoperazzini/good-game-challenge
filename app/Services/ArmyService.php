<?php

namespace App\Services;

use App\Contracts\Services\ArmyServiceInterface;
use App\Contracts\Services\TroopServiceInterface;
use App\Contracts\Services\TypeTroopServiceInterface;
use App\Contracts\Models\ArmyInterface;
use App\Contracts\Models\TroopInterface;

class ArmyService implements ArmyServiceInterface {
    private ArmyInterface $army;
    private TroopServiceInterface $troopService;
    private TypeTroopServiceInterface $typeTroopService;
    private array $typesTroopsAvailable;
    private int $amountTypesTroopsAvailable;
    private int $amountAvailable;

    public function __construct(ArmyInterface $army, TroopServiceInterface $troopService, TypeTroopServiceInterface $typeTroopService ) {
      $this->army = $army;
      $this->troopService = $troopService;
      $this->typeTroopService = $typeTroopService;
    }

    public function create($amount) : ArmyInterface {
        $this->typesTroopsAvailable = $this->typeTroopService->getTypesTroops();
        $this->amountAvailable = $amount;
        $this->amountTypesTroopsAvailable = sizeof($this->typesTroopsAvailable);

        foreach ($this->typesTroopsAvailable as $troopType) {
          $this->insertTroopIntoArmy($troopType);  
        }
        
        return $this->army;
    }

    private function insertTroopIntoArmy(string $troopType) : void {
      $this->amountTypesTroopsAvailable--;

      if ($this->amountTypesTroopsAvailable == 0) {
        $minAmount = $this->amountAvailable;
      } else {
        $minAmount = TroopInterface::MIN_AMOUNT;
      }

      $troop = $this->troopService->create(
        $minAmount,
        $this->amountAvailable - $this->amountTypesTroopsAvailable,
        new $troopType()
      );
      
      $this->amountAvailable -= $troop->getAmount();
      $this->army->addTroop($troop);
    }

}