<?php

namespace App\Services;

use App\Contracts\Services\TypeTroopServiceInterface;

class TypeTroopService implements TypeTroopServiceInterface {
    public function getTypesTroops() : array {
        $types = [];
        $dirs = scandir(app_path().'/Models/Troops/');
        foreach ($dirs as $dir) {
            if($dir != '.' && $dir != '..'){
                $types[] = $this->sanitize(app_path().self::MODEL_DIR.$dir);
            }
        }
        return $types;
    }

    private function sanitize(string $str) : string {
        $str = str_replace('.php', '', $str);
        $str = str_replace(app_path(), '', $str);
        $str = str_replace('/', '\\', $str);
        $str = '\App'.$str;
        return $str;
    }
}