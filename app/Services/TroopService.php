<?php

namespace App\Services;

use App\Contracts\Services\TroopServiceInterface;
use App\Contracts\Models\TroopInterface;

class TroopService implements TroopServiceInterface {
    private TroopInterface $troop;

    public function create($minAmount, $maxAmount, $troop) : TroopInterface {
        $amount = rand($minAmount, $maxAmount);
        $this->troop = $troop;
        $this->troop->setAmount($amount);
        return $this->troop;
    }
}