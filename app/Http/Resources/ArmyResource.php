<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ArmyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $return = [];
        $army = $this->resource;

        foreach($army->getTroops() as $troop){
            $return[$troop->getType()] = $troop->getAmount();
        }

        return $return;
    }
}
