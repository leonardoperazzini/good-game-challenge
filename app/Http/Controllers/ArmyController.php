<?php

namespace App\Http\Controllers;

use App\Contracts\Services\ArmyServiceInterface;
use App\Http\Requests\Army\CreateRequest;
use App\Http\Resources\ArmyResource;

class ArmyController extends Controller{
    private $armyService;

    public function __construct(ArmyServiceInterface $armyService ) {
      $this->armyService = $armyService;
    }

    public function create(CreateRequest $req){
      $army = $this->armyService->create($req->amount); 
      $army = new ArmyResource($army);
      return response()->json($army, 201);
    }
}