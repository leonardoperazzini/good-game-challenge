<?php

namespace App\Http\Requests\Army;

use App\Contracts\Services\TypeTroopServiceInterface;
use App\Contracts\Requests\Army\CreateRequestInterface;
use App\Http\Requests\FormRequest; 
use Illuminate\Validation\Rule;

class CreateRequest extends FormRequest implements CreateRequestInterface {
    public function authorize(){
        return true;
    }

    public function rules(){ 
        return [
            'amount' => 'required|integer'
        ];
    }

    public function setTypeTroopServiceInterface(TypeTroopServiceInterface $typeTroopService) {
        $this->typeTroopService = $typeTroopService;
    }

    public function withValidator($validador)
    {
        $validador->after(function ($validador) { 
            $data = $validador->getData(); 
            $typesTroopsAvailable = $this->typeTroopService->getTypesTroops();
            if(isset($data['amount']) && is_int($data['amount']) && sizeof($typesTroopsAvailable) > $data['amount']){
                $validador->errors()->add('amount', 'Must be bigger than '.$data['amount'] );
            }
        });
    }
}
