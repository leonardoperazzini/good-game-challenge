<?php

namespace App\Contracts\Requests\Army;

use App\Contracts\Services\TypeTroopServiceInterface;

interface CreateRequestInterface {
  public function setTypeTroopServiceInterface(TypeTroopServiceInterface $typeTroopService);
}