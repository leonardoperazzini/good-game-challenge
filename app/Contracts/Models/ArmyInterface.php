<?php

namespace App\Contracts\Models;

use App\Contracts\Models\TroopInterface;

interface ArmyInterface {
  public function addTroop(TroopInterface $troop) : void;
  public function getTroops() : array;
}