<?php

namespace App\Contracts\Models;

interface TroopInterface {
  CONST TYPE = 'gerenic';
  CONST MIN_AMOUNT = 1;

  public function getType() : string;
  public function getAmount() : int;
  public function setAmount(int $amount) : void;
}