<?php

namespace App\Contracts\Services;

use App\Contracts\Models\TroopInterface;

interface TroopServiceInterface {
    public function create(int $minAmount, int $maxAmount, TroopInterface $troop) : TroopInterface;
}