<?php

namespace App\Contracts\Services;

interface TypeTroopServiceInterface {
  CONST MODEL_DIR = '/Models/Troops/';

  public function getTypesTroops() : array;
}