<?php

namespace App\Contracts\Services;

use App\Contracts\Models\ArmyInterface;

interface ArmyServiceInterface {
    public function create(int $amount) : ArmyInterface;
}