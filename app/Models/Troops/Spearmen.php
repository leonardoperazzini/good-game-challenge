<?php

namespace App\Models\Troops;

use App\Models\Troop;

class Spearmen extends Troop {
  CONST TYPE = 'spearmen';

  public function __construct() {
    $this->type = self::TYPE;
  }
}
