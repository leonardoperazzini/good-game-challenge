<?php

namespace App\Models\Troops;

use App\Models\Troop;

class Archer extends Troop {
  CONST TYPE = 'archer';

  public function __construct() {
    $this->type = self::TYPE;
  }
}
