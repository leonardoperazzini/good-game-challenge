<?php

namespace App\Models\Troops;

use App\Models\Troop;

class Swordsmen extends Troop {
  CONST TYPE = 'swordsmen';

  public function __construct() {
    $this->type = self::TYPE;
  }
}
