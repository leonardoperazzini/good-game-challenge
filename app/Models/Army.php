<?php

namespace App\Models;

use App\Contracts\Models\TroopInterface;
use App\Contracts\Models\ArmyInterface;

class Army implements ArmyInterface {
    protected array $troops;

    public function addTroop(TroopInterface $troop) : void {
        $this->troops[] = $troop;
    }

    public function getTroops() : array {
        return $this->troops;
      }
    
}
