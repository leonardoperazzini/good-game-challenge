<?php

namespace App\Models;

use App\Contracts\Models\TroopInterface;

abstract class Troop implements TroopInterface{
  protected string $type = self::TYPE;
  protected int $amount;

  public function getType() : string {
    return $this->type;
  }

  public function getAmount() : int {
    return $this->amount;
  }

  public function setAmount(int $amount) : void {
    $this->amount = $amount;
  }
}
